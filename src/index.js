import * as THREE from 'three';
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import {RenderPass} from 'three/examples/jsm/postprocessing/RenderPass.js';
import {BloomPass} from 'three/examples/jsm/postprocessing/BloomPass.js';
import {GlitchPass} from 'three/examples/jsm/postprocessing/GlitchPass.js';
import {UnrealBloomPass} from 'three/examples/jsm/postprocessing/UnrealBloomPass.js';

function makering(radius, polyedges, color, end, pos = {x: 0, y: 0}) {
    const curve = new THREE.EllipseCurve(
	pos.x, pos.y,    // aX, aY
	radius, radius,   // xRadius, yRadius
	0, end, // aStartAngle, aEndAngle
	true,       // aClockwise
	0,          // aRotation
    );

    const points = curve.getPoints( polyedges );
    const geometry = new THREE.BufferGeometry().setFromPoints( points );
    const material = new THREE.LineBasicMaterial( { color } );

    return new THREE.Line( geometry, material );
}

function main() {
    var scene = new THREE.Scene();
    var camera = new THREE.PerspectiveCamera(
	75,
	window.innerWidth / window.innerHeight,
	0.1,
	1000
    );

    var renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );

    const polyedges = 12;
    const yellow = 0xffcc00;

    var face = makering( 3, polyedges, yellow, 2*Math.PI);
    var eye1 = makering( 0.5, polyedges - 4, yellow, 2*Math.PI, {x:-1, y:1});
    var eye2 = makering(0.5, polyedges - 4, yellow, 2*Math.Pi, {x:1, y:1});
    var smile = makering(1.5, polyedges - 6, yellow, Math.PI, {x:0, y:-0.5});

    var smileyface = new THREE.Group();
    smileyface.add(face);
    smileyface.add(eye1);
    smileyface.add(eye2);
    smileyface.add(smile);

    scene.add(smileyface);

    camera.position.z = 7; // Zoom out a bit

    const composer = new EffectComposer(renderer);
    composer.addPass(new RenderPass(scene, camera));

    const unrealBloompass = new UnrealBloomPass(
	256, // resolution
	4,   // strength
	0,   // radius
	0,   // threshold
    );
    composer.addPass(unrealBloompass);
    const glitchPass = new GlitchPass(0.2);
    composer.addPass(glitchPass);
    
    const speed = 0.015;
    function animate() {
	requestAnimationFrame( animate );
	smileyface.rotation.y += speed;
	composer.render();
    }
    animate();
}

main();
